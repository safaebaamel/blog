import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from '@nestjs/common';
import { Observable } from 'rxjs';
import { User } from '../models/user.interface';
import { UserService } from '../service/user.service';

@Controller('users')
export class UserController {

    constructor(private userService: UserService){}

    @Get(':id')
    findOne(@Param() params): Observable<any>  {
        return this.userService.findOne(params.id);
    }

    @Get()
    findAll(@Param() params): Observable<User[]>  {
        return this.userService.findAll();
    }

    @Post()
    create(@Body() user: User): Observable<User> {
        return this.userService.create(user);
    }

    @Put(':id') 
    update(@Param('id') id:string, @Body() user: User): Observable<User>  {
        return this.userService.updateOne(Number(id), user);
    }

    @Delete(':id')
    deleteOne(@Param('id') id: string ): Observable<any>  {
        return this.userService.deleteOne(Number(id));
    }

}
